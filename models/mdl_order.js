const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
	userId : {
		type : String,
		required : [true, "User ID is required."]
	},
	
	orderedOn: {
		type: Date,
		default : new Date()
	},
	
	orderQuantity: {
		type: Number,
		required : [true, "Order quantity is required."]
	},
	
	productId: {
		type : String,
		required : [true, "Product ID is required."]
	},

	totalAmount: {
		type : Number,
		required : [true, "Total amount is required."]
	}
});

module.exports = mongoose.model("Order", orderSchema);