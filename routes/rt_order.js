const express = require('express');
const auth = require('../auth');
const orderController = require('../controllers/ctrl_order');
const router = express.Router();

// Route for creating an order/checking out (NON-ADMIN ONLY)
router.post('/users/checkout', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.createOrder(userData, req.body).then(result => res.send(result));
})

// Route for getting all orders (ADMIN ONLY)
router.get('/users/orders', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.getAllOrders(userData).then(result => res.send(result));
})

// Route for getting a user's order/s (NON-ADMIN ONLY)
router.get('/users/myOrders', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.getMyOrders(userData, req.body).then(result => res.send(result));
})

module.exports = router;