const Product = require("../models/mdl_product");
const auth = require("../auth");

// Controller for creating a product (ADMIN ONLY)
module.exports.createProduct = async (reqBody, userData) => {
	if(userData.isAdmin){
		let newProduct = new Product ({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price,
			stocks : reqBody.stocks
		})
		return newProduct.save().then((user, error) => {
			if(error){
				return `Cannot add product to the database.`;
			} else {
				return `Product added to the database.`;
			}
		})
	} else {
		return `You are not authorized to do this action.`
	}
	
}

// Controller for retrieving all active products
module.exports.getAllProducts = () => {
	return Product.find({isActive : true}).then(result => {
		if(result == null || result == undefined) {
	    return "Product does not exist."
	  }
	  else {
      return result;
    }
	})
}

// Controller for retrieving a specific product
module.exports.getProduct = (reqParams) => {
	return Product.findOne({_id : reqParams.productId}).then(result => {
		return result;
	}).catch((error) => {
		return `Product does not exist.`
	})
}

// Controller for updating a product (ADMIN ONLY)
module.exports.updateProduct = async (productId, reqBody, isAdmin) => {
	if(isAdmin){
		let updatedProduct = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price,
			stocks : reqBody.stocks,
			isActive : reqBody.isActive
		}
		return Product.findByIdAndUpdate(productId, updatedProduct)
		.then((promise, error) => {
			if(error){
				return false;
			}
			else{
				return `Product details updated.`;
			}
		})
	}
	else{
		return `You are not authorized to do this action.`;
	}
}

// Controller for archiving a product (ADMIN ONLY)
module.exports.archiveProduct = async (userData, reqParams) => {
	if(userData.isAdmin){
		return Product.findByIdAndUpdate({_id : reqParams.productId}, {isActive: false}).then(result => {
			return result.save().then((updatedResult, error) => {
				if(error){
					return `Error: Archiving failed.`
				} else {
					return `Archiving successful. ${updatedResult}`;
				}
			});
		})
	}
	else{
		return `You are not authorized to do this action.`
	}
}
