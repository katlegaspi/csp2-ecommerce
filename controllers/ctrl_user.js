const User = require("../models/mdl_user");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Controller for checking if the email already exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result =>{
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

// Controller for retrieving all users (ADMIN ONLY)
module.exports.getAllUsers = async (userData) => {
	if(userData.isAdmin){
		return User.find().then(result => {
			if(result === null || result === undefined) {
	      return `User/s do not exist.`
	    }
	    else{
      	for(let i = 0; i < result.length; i++){
      		result[i].password = "CONFIDENTIAL";
      	}
        return result;
      }
		})
	}
	else{
		return `You are not authorized to do this action.`
	}
}

// Controller for User Registration | Sign up
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error){
			return `Sign up failed.`;
		} else {
			return `Sign up successful!`;
		}
	})
}

// Controller for User authentication | Log in
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return `Email does not exist. Sign up as a user to log in.`;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
				}
			else{
				return `Incorrect password!`;
			}
		}	
	})
}

// Controller for setting first admin access
module.exports.setFirstAdmin = (userId) => {
	return User.findById(userId).then((result, error) => {
		if(error){
			return false;
		}
		result.isAdmin = true;

		return result.save().then((updateUser, err) => {
			if(error){
				return false;
			}
			else{
				return `User access upgraded to admin.`;
			}
		})
	})
}

// Controller for setting admin access
module.exports.setAsAdmin = async (userData, userId, reqBody) => {
	if(userData.isAdmin) {
		return await User.findById(userId).then((result,error) =>{
			if(error){
				return "User does not exist."
			}

			else{
				result.isAdmin = reqBody.isAdmin;
				return result.save().then((user,error) => {
					if (error){
						return false;
					}
					else {
						return `Access for ${result.email} has been updated.`;
					}
				})
			}
		})
	}
	else{
		return "You are not authorized to do this action.";
	}
}